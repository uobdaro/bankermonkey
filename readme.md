#bankermonkey

Utilities to support financial processes in Blackbaud CRM. These currently include:

* DDM Integration global changes

* Purpose Directory forms and tables

##Getting Started
These instructions will help install the package and detail further configuration required within CRM.

###Prerequisites
* CRM SP20

###Installing

Download the package file into *vroot\bin\custom*.

Open the catalog browser and load the *bankermonkey package* spec.

Refresh shell navigation.

###Additional Configuration

Add security permissions to relevant role for the following:

* Purpose Directory Edit

* Purpose Directory View

##Testing
*add UAT script refs*

##Built With
* [Infinity SDK](https://www.blackbaud.com/files/support/guides/infinitytechref/infrefversions-developer-help.htm) - Blackbaud CRM SDK

##Authors
* Luke McGarrity - initial work
